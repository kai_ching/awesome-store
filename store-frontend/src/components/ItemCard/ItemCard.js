import React, {useContext} from "react";
import Button from "@material-ui/core/Button";

import "./ItemCard.scss";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import {useDispatch} from "react-redux";

import { ActionTypes } from "../ShoppingCart/shoppingCartReducer"
import {AppConfig} from "../../App";
import {callEndpoint} from "../../APIUtils";

const ItemCard = ({item, searchTerm, requestId}) => {
  const { endpoints, elastic } = useContext(AppConfig);

  const clickEndpoint = endpoints.click
    .replace("{query}", searchTerm)
    .replace("{id}", item.id.raw)
    .replace("{requestId}", requestId)

  const dispatch = useDispatch();

  const addItem = async () => {
    dispatch({
      type: ActionTypes.ADD_ITEM,
      payload: item
    });
    try {
      await callEndpoint(clickEndpoint, {
        headers:{
          Authorization: `Bearer ${elastic.apiKey}`
        }
      }, "POST");
    } catch(error) {
      // do nothing
    }
  }

  return (
    <Card className="item-card">
      <div>
        <CardMedia className="item-image"
          image= { item.image_url ? item.image_url.raw : null }
          title={item.title.raw}
        />
        <CardContent className="item-detail">
          <Typography gutterBottom variant="h5" component="h2">
            {item.title ? item.title.raw : null}
          </Typography>
          <span>${ item.unit_price ? item.unit_price.raw : null }</span>
        </CardContent>
      </div>
      <CardActions>
        <Button size="small" color="primary" onClick={addItem}>
          Add to cart
        </Button>
      </CardActions>
    </Card>
  )
}

export default ItemCard;
