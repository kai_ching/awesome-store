import React, {useState} from "react";
import {CircularProgress, debounce, TextField} from "@material-ui/core";
import {callEndpoint} from "../../APIUtils";

import inventory from "../../workspace/pages/search/inventory.json";

const elasticLookup = (endpoint, apiKey, searchTerm) => {
  endpoint = endpoint.replace("{query}", searchTerm);
  return callEndpoint(endpoint, {
    headers:{
      "Authorization": `Bearer ${apiKey}`
    }
  }).then(response =>  {
    return response;
  });
}

const SearchBar = ({endpoint, apiKey, onChange}) => {
  const [loading, setLoading] = useState(false);

  const performSearch = debounce((searchTerm) => {
      if(searchTerm && searchTerm.length > 0) {
        setLoading(true);
        elasticLookup(endpoint, apiKey,searchTerm)
          .then(response => {
            if (typeof onChange === "function") {
              onChange({searchTerm, response});
            }
          })
          .finally(() => {
            setLoading(false)
          });
      }
    });

  return (
    <TextField
      variant="outlined"
      placeholder="Search for an item..."
      fullWidth
      InputProps={{
        endAdornment: (
          <React.Fragment>
            {loading ? <CircularProgress color="inherit" size={20}/> : null}
          </React.Fragment>
        ),
      }}
      onChange={(event) => {
        performSearch(event.target.value);
      }}
    />
  );
}

export default SearchBar;
