import React, { useEffect } from "react";

import "./ConformationPage.scss";
import {useDispatch, useSelector} from "react-redux";
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router-dom";
import {ActionTypes} from "../../../components/ShoppingCart/shoppingCartReducer";

const ConfirmationPage = () => {
  const history = useHistory();
  const receipt = useSelector(store => store.payment.receipt);
  const amountPaid = useSelector(store => store.payment.amount);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: ActionTypes.CLEAR_BASKET
    });
  }, []);

  return (
    <div className="confirmation">
      <p>
        Thank you for shopping at Elastic Pets. You have paid an amount of {amountPaid}
      </p>
      <p>
        Your receipt number is <b>{receipt}</b>.
      </p>

      <div className="buttons">
        <Button color="primary" size="medium" variant="contained"
                onClick={() => { window.location.href = "/"}}>
          Back to shopping
        </Button>
      </div>
    </div>);
}

export default ConfirmationPage;
