import React, { useState, useContext } from "react";
import ItemCard from "../../../components/ItemCard/ItemCard";
import {AppConfig} from "../../../App";
import "./SearchPage.scss";
import SearchBar from "../../../components/Search/SearchBar";
import {Box, Typography} from "@material-ui/core";
import {Label} from "@material-ui/icons";

const SearchPage = () => {
  const { endpoints, elastic } = useContext(AppConfig);
  const [items, setItems] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [requestId, setRequestId] = useState("");

  const handleSearchChange = (search) => {
    const { meta, results } = search.response;
    setRequestId(meta.request_id);
    setSearchTerm(search.searchTerm);
    setItems(results);
  };

  const hasResults = Array.isArray(items) && items.length > 0;
  const noResults = typeof searchTerm === "string" && searchTerm.length > 0 && !hasResults;

  return (
    <div className="page-search">
      <Box py={1}>
        <SearchBar endpoint={endpoints.search} apiKey={elastic.apiKey} onChange={handleSearchChange}/>
      </Box>
      {hasResults ? (
        <Box py={1}>
          <div className="item-results">
            {items.map((item, index) => <ItemCard
              key={index}
              item={item}
              requestId={requestId}
              searchTerm={searchTerm}
            />)
            }
          </div>
        </Box>
      ) : null}

      {noResults ? (
        <Box
          py={1}
          display="flex"
          alignItems="center"
          justifyContent="center"
          flex="1 1 auto"
        >
          <Typography variant="h6">
            No results found
          </Typography>
        </Box>
      ): null}
    </div>

  );
}

export default SearchPage;
