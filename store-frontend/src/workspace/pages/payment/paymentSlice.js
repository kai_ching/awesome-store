import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {callEndpoint} from "../../../APIUtils";

export const createOrder = createAsyncThunk("payment/createOrder", (params) => {
  const { items, amount } = params;
  return callEndpoint("/api/orders", {
    body :{
      items: items,
      amount: amount
    }
  }, "POST");
});

export const processPayment = createAsyncThunk("payment/processPayment", (params) => {
  const { orderId, cardDetail, address } = params;
  return callEndpoint("/api/payment", {
    body :{
      orderId: orderId,
      cardDetail: cardDetail,
      address: address
    }
  }, "PUT")
});

export const PaymentSlice = createSlice({
  name: "payment",
  initialState: {
    orderId: null,
    receipt: null,
    status: null
  },
  extraReducers: {
    [createOrder.fulfilled]: (state, action) => {
      state.orderId = action.payload.orderId;
    },
    [processPayment.fulfilled]: (state, action) => {
      state.status = "success";
      state.receipt = action.payload.receipt;
    },
    [processPayment.rejected]: (state, action) => {
      state.status = "failed";
      state.message = action.payload.message;
    }
  }
});

export default PaymentSlice.reducer;


