import React, {useContext, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import { TextField, FormControl } from "@material-ui/core";

import "./PaymentPage.scss";
import Button from "@material-ui/core/Button";

import { callEndpoint} from "../../../APIUtils";
import {AppConfig} from "../../../App";
import {ActionTypes} from "./orderReducer";
import Invoice from "./Invoice";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Alert, AlertTitle } from '@material-ui/lab';
import {processPayment} from "./paymentSlice";

const formatCreditCardNumber = cardNumber => {
  cardNumber = cardNumber.replaceAll("-", "");
  const blocks = cardNumber.match(/.{1,4}/g);
  if(blocks) {
    return blocks.join("-");
  } else {
    return "";
  }
}

const validateCreditCardNumberLength = cardNumber => {
  cardNumber = cardNumber.replaceAll("-", "");
  return cardNumber.length < 17;
}

const validateCreditCardInput = cardNumber => {
  cardNumber = cardNumber.replaceAll("-", "");
  return cardNumber.length === 0 || /^\d+$/.test(cardNumber);
}

const formatExpiry = expiry => {
  expiry = expiry.replaceAll("/", "").slice(0, 4);
  if(expiry.length > 2) {
    return [expiry.slice(0, 2), "/", expiry.slice(2)].join('');
  } else {
    return expiry;
  }
}

const validateExpiry = expiry => {
  expiry = expiry.replaceAll("/", "");
  return expiry.length === 0 || /^\d+$/.test(expiry);
}

const validateCCV = ccv => (ccv.length === 0 || /^\d+$/.test(ccv));


const PaymentPage = () => {
  const { endpoints } = useContext(AppConfig);
  const dispatch = useDispatch();
  const history = useHistory();

  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [postCode, setPostCode] = useState("");

  const [cardNumber, setCardNumber] = useState("");
  const [cardholderName, setCardholderName] = useState("");
  const [expiry, setExpiry] = useState("");
  const [ccv, setCCV] = useState("");

  const basket = useSelector(store => store.cart.basket);
  const total = useSelector(store => store.cart.total);
  const itemCount = useSelector(store => store.cart.itemCount);

  const status = useSelector(store => store.payment.status);
  const message = useSelector(store => store.payment.message);

  const orderId = useSelector(store => store.payment.orderId);
  const receipt = useSelector(store => store.payment.receipt);

  const loading = useSelector(store => store.status.loading);
  const submitting = loading.indexOf("placeOrder") > -1 || loading.indexOf("processOrder") > -1;


  useEffect(() => {
    if(itemCount === 0) {
      history.push("/");
    }
  }, []);

  useEffect(() => {
    if(status === "success" || status === "processed") {
      history.push("/confirmation");
    }
  }, [status]);

  const submitPayment = () => {
    dispatch(processPayment({
      orderId: orderId,
      cardDetail: {
        cardNumber: cardNumber,
        cardholderName: cardholderName,
        expiry: expiry
      },
      address: {
        address: address,
        city: city,
        postCode: postCode
      }
    }));
  }

  const cancel = () => {
    history.push("/");
  }

  return (
    <div className="page-payment">
      <div className="side-bar">
        <Invoice basket={basket} total={total}/>
      </div>

      <div className="main-content">
        <form
          onSubmit={(event) => {
            event.preventDefault();
            submitPayment();
            return false;
          }}
          onReset={(event) => {
            cancel();
            event.preventDefault();
          }}>
          <h3>Shipping Address</h3>
          <FormControl className="address" fullWidth margin="normal">
            <TextField name="address" label="Address" variant="outlined"
                       value={address} onChange={(event) => { setAddress(event.target.value)}} />
          </FormControl>
          <FormControl className="city" margin="normal">
            <TextField name="city" label="City" variant="outlined"
              value={city} onChange={(event) => { setCity(event.target.value)}}/>
          </FormControl>
          <FormControl className="postCode" margin="normal">
            <TextField name="postCode" label="Post Code" variant="outlined"
              value={postCode} onChange={(event) => { setPostCode(event.target.value)}}/>
          </FormControl>

          <h3>Payment Details</h3>
          <FormControl className="cardNumber" fullWidth margin="normal">
            <TextField name="cardNumber" label="Card Number" variant="outlined"
                       error={!validateCreditCardNumberLength(cardNumber)}
                       helperText={!validateCreditCardNumberLength(cardNumber) ? "Credit card number too long" : null}
              value={cardNumber} onChange={
                (event) => {
                  if(validateCreditCardInput(event.target.value)) {
                    setCardNumber(formatCreditCardNumber(event.target.value));
                  }
                }}/>
          </FormControl>
          <FormControl className="cardholderName" margin="normal">
            <TextField name="cardholderName" label="Cardholder Name" variant="outlined"
              value={cardholderName} onChange={(event) => { setCardholderName(event.target.value)}}/>
          </FormControl>
          <FormControl className="expiry" margin="normal">
            <TextField name="expiry" label="Expiry Date" variant="outlined"
              value={expiry} onChange={(event) => {
                if(validateExpiry(event.target.value)) {
                  setExpiry(formatExpiry(event.target.value))}
                }
              }/>
          </FormControl>
          <FormControl className="ccv" margin="normal">
            <TextField name="ccv" label="CCV" variant="outlined"
              value={ccv} onChange={(event) => {
                if(validateCCV(event.target.value)) {
                  setCCV(event.target.value.slice(0,3))
                }
              }}
            />
          </FormControl>

          { status === "error" || status === "failed" ? (
            <Alert severity="error" className="error">
              <AlertTitle>Error</AlertTitle>
              {message}
            </Alert>
          ) : null}

          { status === "success" ? (
            <Alert severity="success">
              <AlertTitle>Your order has been processed. Your receipt number is ${receipt}.</AlertTitle>
            </Alert>
          ) : null}

          <div className="buttons">
            <span style={{display: "flex", alignItems:"center"}}>
              <Button type="submit" color="primary" size="large" variant="contained"
                      disabled={submitting}>
                Confirm Payment
              </Button>
              <CircularProgress size={24} className={`progress ${submitting?"submitting":null}`}/>
            </span>

            <Button type="reset" size="large" variant="contained" disabled={submitting}>
              Cancel
            </Button>
          </div>
        </form>
      </div>

    </div>
  );
}

export default PaymentPage;
