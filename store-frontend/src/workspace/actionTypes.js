export const FILE_UPLOADING = 'fileUploading';
export const FILE_UPLOADED = 'fileUploaded';

export const PROPERTY_AVAILABLE = 'availableProperties';
export const PROPERTY_SUBMITTED = 'propertySubmitted';

export const ENTITY_IDENTIFIED = 'identifiedEntity';

export const PROCESSING_COMPLETED = 'processingCompleted';

export const NOTIFICATION_UPDATE = 'showMessage';
export const NOTIFICATION_CLEAR = 'clearMessage';
