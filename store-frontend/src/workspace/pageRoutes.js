import OfferPage from "./pages/offer/OfferPage";
import PaymentPage from "./pages/payment/PaymentPage";
import ConfirmationPage from "./pages/confirmation/ConfirmationPage";
import SearchPage from "./pages/search/SearchPage";

export const pageRoutes = [
  {
    path: "/",
    label: "offer",
    pageContent: SearchPage
  },
  {
    path: "/payment",
    label: "payment",
    pageContent: PaymentPage,
    props: {
      hideCart: true
    }
  },
  {
    path: "/confirmation",
    label: "confirmation",
    pageContent: ConfirmationPage,
    props: {
      hideCart: true
    }
  },
  {
    path: "/search",
    label: "search",
    pageContent: SearchPage,
    props: {}
  }
]

export const defaultRoute = "/search";
