const defaultOptions = {
  headers: {
    "Accept": "application/json",
    "Content-Type": "application/json"
  }
}

const apiRoot = document.head.querySelector("link[rel='api-root']");
let baseUrl = undefined;
if(apiRoot) {
  baseUrl = apiRoot.getAttribute("href");
  if(baseUrl && baseUrl.charAt(baseUrl.length - 1) === "/") {
    baseUrl = baseUrl.slice(0, baseUrl.length - 1);
  }
}

function _callEndpoint(endpoint, options={}, method="GET") {
  const mergedOptions = {...defaultOptions, ...options};
  mergedOptions.headers = { ...defaultOptions.headers, ...options.headers}
  mergedOptions.method = method;

  // TODO: work out better serialization
  if(typeof mergedOptions.body === "object" && mergedOptions.headers) {
    const contentType = mergedOptions.headers["Content-Type"];
    if (contentType === "application/json") {
      mergedOptions.body = JSON.stringify(mergedOptions.body);
    }
  }

  if(mergedOptions.headers) {
    mergedOptions.headers = new Headers(mergedOptions.headers)
  }

  return fetch(endpoint, mergedOptions)
    .then(response => {
      if(!response.ok) {
        return response.json().then(responseBody => {
            throw {
              code: response.status,
              message: responseBody.message
            }
          });
      } else {
        return response;
      }
    })
    .then(response => {
      const contentType = response.headers.get("content-type");
      if (contentType && contentType.indexOf("application/json") !== -1) {
        return response.json();
      } else {
        return response.text();
      }
    })
    .catch(error => {
      throw error
    });
}


export function callEndpoint(endpoint, options, method) {
  if(baseUrl && endpoint.charAt(0) === "/") {
    endpoint = baseUrl + endpoint;
  }
  return _callEndpoint(endpoint, options, method);
}
