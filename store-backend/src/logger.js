const { createLogger, transports, format} = require("winston");
const { combine, timestamp, prettyPrint, printf } = format;

const config = require("config");
const ecsFormat = require("@elastic/ecs-winston-format");
const winston = require("winston");

const lineFormat = printf(
  ({timestamp, level, service, label, message}) =>
    `[${timestamp} | ${level.toUpperCase()} | ${service.name} | ${label ? label : ""} ]  ${message}`
);

const childLoggers = {};

const rootLogger = createLogger({
  level: config.logging.level,
  defaultMeta: {
    service: {
      name: "ElasticPets"
    }
  },
  transports: [
    new winston.transports.File({
      filename: "logs/log.json",
      format: ecsFormat({ convertReqRes: true})
    }),
    new transports.Console({
      format: combine(
        timestamp({format: "YYYY-MM-DD HH:mm:ss.SSS"}),
        prettyPrint(),
        lineFormat
      )
    })
  ]
});

module.exports = (label) => {
  if(label) {
    let serviceLogger = childLoggers[label];
    if(!serviceLogger) {
      serviceLogger = rootLogger.child({ label: label});
      childLoggers[label] = serviceLogger;
    }
    return serviceLogger;
  } else {
    return rootLogger;
  }
}
