const http = require("http");
const express = require("express");
const config = require("config");
const logger = require("./logger")("app");
const cors = require("cors");

const router = express.Router();
router.use(cors());
require("./routes")(router);

const app = express();
app.use(express.json());
app.use(config.app.basePath ?? "/", router);
const port = config.app.port;

const httpServer = http.createServer(app);

httpServer.listen(port, () => {
  logger.info(`ElasticPets server listening on port ${port}`);
});
