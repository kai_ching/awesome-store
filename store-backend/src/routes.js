const createOrder = require("./createOrder");
const processPayment = require("./processPayment");


module.exports = (router) => {
  router.post("/orders", createOrder);
  router.put("/payment", processPayment);
}
