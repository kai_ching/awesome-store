const logger = require("./logger")("fulfillment");

const despatch = (orderId, items, destination) => {
  const { address, city, postCode} = destination;
  logger.info(`order_id=${orderId} | Shipping to: address="${address}" city="${city}" postcode="${postCode}"`);
}

module.exports = {
  despatch
};
