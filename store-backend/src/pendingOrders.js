class OrdersStore {
  constructor() {
    this.orders = {};
  }

  getOrder(id) {
    return this.orders[id];
  }

  putOrder(order) {
    if(order && order.id) {
      this.orders[order.id] = order;
      return order.id;
    }
  }

  removeOrder(id) {
    if(typeof id === "string") {
      delete this.orders[id];
    }
  }
}

module.exports = new OrdersStore();
