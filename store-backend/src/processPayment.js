const logger = require("./logger")("payment");
const orders = require("./pendingOrders");
const { despatch } = require("./fulfillment");

const sleep = async(ms = 0) => {
  return new Promise(resolve => {
    setTimeout(() => resolve(), ms);
  });
}

module.exports = async (req, res) => {
  const { orderId, address } = req.body;
  const order = orders.getOrder(orderId);

  if(order) {
    const { amount, items } = order;
    orders.removeOrder(orderId);

    logger.info(`order_id=${orderId} | Processing payment. `)
    await sleep(1000);
    logger.info(`order_id=${orderId} | Payment successful. amount=${amount}`);

    items.forEach(item => {
      logger.info(`item_id=${item.id.raw} | Item sold. quantity=${item.quantity}`);
    });
    despatch(orderId, items, address);

    res.status(200).json({
      orderId: orderId,
      amount: amount,
      receipt: Math.random().toString(10).slice(2, 10)
    }).end();
  } else {
    logger.error(`order_id=${orderId} | Invalid order ID`);
    res.status(400).json({
      message: `Invalid order Id`,
      orderId: orderId
    });
  }
}
