const logger = require("./logger")("orders");
const orders = require("./pendingOrders");

module.exports = (req, res) => {
  const orderId = Math.random().toString(36).slice(2);
  orders.putOrder({
    id: orderId,
    items: req.body.items,
    amount: req.body.amount
  });

  logger.info(`order_id=${orderId} | Order created.`);
  res.status(201).json({
    orderId: orderId
  }).end();
}
