How to use this demo
---

Back End
---
1. npm install
2. npm run start

Front End
---
1. npm install
2. Modify the hostname for search and click endpoints defined in index.html
3. Replace the elastic API key for your own instance
4. npm run start
5. Fire up browser at http://localhost:3000

FileBeat config
---
1. Download [filebeat](https://www.elastic.co/downloads/beats/filebeat)
2. Edit filebeat.yml
3. Specify cloud.id
4. Specify cloud.auth
5. under filestreams, modify path to point to log.json
